<?php 

require("../partials/template.php");

function get_title(){
	echo "Profile";
};

function get_body_contents(){
require("../controllers/connection.php");

$userId = $_SESSION['user']['id'];
?>

<h1 class="text-center py-3">Profile Page</h1>
<div class="container">
	<div class="row">
		<div class="col-lg-6">
			<!-- Profile Details -->
			<div class="card">
				<div class="card-img  text-center">

							<?php 
					$image_query = "SELECT * FROM users WHERE id = $userId";
					$image = mysqli_fetch_assoc(mysqli_query($conn,$image_query));
					if($image){
					?>
					<img src="../assets/images/profile_pic/
					<?=$image['image'] ?>
					" alt="Profile" height="200px" width="200px" class="rounded-circle id="profileImg"">
					<?php
					}
					?>
				
					
				</div>
				<div class="card-footer">
					<form action="../controllers/upload-process.php" enctype="multipart/form-data" method="POST">
						<div class="form-group">
							<label for="profile">Upload Image</label>
							<input type="file" name="profile" class="form-control-file" id="preview">
						</div>
						<button class="btn btn-secondary" type="submit">Upload</button>
					</form>

				</div>
			</div>
			<div class="card">
				<div class="card-body">
					<h4 class="card-title text-center py-3">Contacts:</h4>
				</div>
					<?php 

					$contacts_query = "SELECT * FROM contacts WHERE user_id = $userId";
					$contacts = mysqli_query($conn,$contacts_query);
					foreach ($contacts as $indiv_contact) {
					?>
					<div class="card-content">
						<p class="text-center"><?= $indiv_contact['contactNo'];?></p>
					</div>
			
					<?php
					}
					 ?>
			</div>

			<div class="card">
				<div class="card-body">
					<h4 class="card-title text-center">Addresses</h4>
				</div>
				
				<?php
					$address_query = "SELECT * FROM addresess WHERE user_id = $userId";
					$addresses = mysqli_query($conn,$address_query);
					foreach($addresses as $indiv_address) {
				?>
				<div class="card-content">
					<p class="text-uppercase text-center">
						<?= 
						$indiv_address['address1']." , ".$indiv_address['address2']. " , " .$indiv_address['zip_code'];
						?>
					</p>
				</div>
				<?php
					}

				 ?>

			</div>
		</div>
		<div class="col-lg-6">
			<h1>Addresess:</h1>
			<form action="../controllers/add-address-process.php" method="POST">
				<div class="form-group">
					<label for="address1">Address 1:</label>
					<input type="text" name="address1" class="form-control">
				</div>
				<div class="form-group">
					<label for="address2">Address 2:</label>
					<input type="text" name="address2" class="form-control">
				</div>
				<div class="form-group">
					<label for="zipCode">Zip Code:</label>
					<input type="number" name="zipCode" class="form-control">
				</div>
				<div class="form-group">
					<label for="city">City :</label>
					<input type="text" name="city" class="form-control">
				</div>
				<input type="hidden" name="user_id" value="<?= $userId ?>">
				<button class="btn btn-secondary" type="submit">Add Address</button>
			</form>

			<form action="../controllers/add-contact-process.php" method="POST">
				<div class="form-group">
					<label for="contact">Add Contact</label>
					<input type="number" name="contact" class="form-control">
				</div>
				<input type="hidden" name="submitContact" value="<?= $userId?>">
				<button type="submit" class="btn btn-secondary">Add</button>
			</form>
		</div>
	</div>
</div>

<script type="text/javascript">


</script>

<?php
}
 ?>

