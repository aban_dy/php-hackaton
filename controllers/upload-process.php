<?php 
	session_start();

	require("connection.php");

	$fileName = $_FILES['profile']['name'];
	$fileType = $_FILES['profile']['type'];
	$tmp_name = $_FILES['profile']['tmp_name'];
	$userId = $_SESSION['user']['id'];

	$tmp_ext = explode(".", $fileName);
	$ext = strtolower(end($tmp_ext));
	$profile_image_name = uniqid('',true). "." . $ext;
	$destination = "../assets/images/profile_pic/";
	$imgPath = $destination . $profile_image_name;

	if (move_uploaded_file($tmp_name, $imgPath)) {
		header("Location:" . $_SERVER['HTTP_REFERER']);
	}else{
		echo "Error";
	}

	$image_query = "UPDATE users SET image = '$profile_image_name' WHERE users.id = $userId";
	$result = mysqli_query($conn,$image_query);


 ?>